# MIAOW

MIAOW stands for `Mower Inputs And Outputs Wrapper`.
It is a simple wrapper to manage inputs and outputs in [Mow Mow].

[mow mow]: https://gitlab.com/mow-mow/mow-mow
